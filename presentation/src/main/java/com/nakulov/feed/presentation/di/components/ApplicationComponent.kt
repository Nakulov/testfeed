package com.nakulov.feed.presentation.di.components

import android.content.Context
import android.content.res.Resources
import com.nakulov.feed.data.NotificationCenter
import com.nakulov.feed.data.cache.Cache
import com.nakulov.feed.data.net.Repository
import com.nakulov.feed.presentation.di.modules.ApplicationModule
import com.nakulov.feed.presentation.service.FeedService
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(ApplicationModule::class)])
interface ApplicationComponent {

    fun context(): Context

    fun resources(): Resources

    fun cache(): Cache

    fun repository(): Repository

    fun notificationCenter(): NotificationCenter

    fun inject(feedService: FeedService)

}