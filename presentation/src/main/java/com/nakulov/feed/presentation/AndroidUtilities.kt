package com.nakulov.feed.presentation

import android.content.Context

const val MATCH_PARENT = -1
const val WRAP_CONTENT = -2

var density = 1.0f

fun calculateDensity(context: Context) {
    density = context.resources.displayMetrics.density
}

fun Int.dip(): Int = Math.floor((density * this).toDouble()).toInt()

fun Float.dip(): Float = Math.floor((density * this).toDouble()).toFloat()

fun cancelRunOnUIThread(runnable: Runnable){
    applicationHandler.removeCallbacks(runnable)
}

inline fun runOnUiThread(crossinline block: () -> Unit, delay: Long = 0) : Runnable {
    val runnable = Runnable {
        block()
    }
    if (delay == 0L)  applicationHandler.post(runnable)
    else applicationHandler.postDelayed(runnable, delay)

    return runnable
}