package com.nakulov.feed.presentation.service

import android.app.IntentService
import android.content.Intent
import android.os.Handler
import android.util.Log
import com.nakulov.feed.data.NotificationCenter
import com.nakulov.feed.data.cache.Cache
import com.nakulov.feed.data.models.Feed
import com.nakulov.feed.data.needUpdateFeed
import com.nakulov.feed.data.net.Repository
import com.nakulov.feed.presentation.FeedApp.Companion.applicationComponent
import com.nakulov.feed.presentation.runOnUiThread
import kotlinx.coroutines.experimental.launch
import javax.inject.Inject

class FeedService : IntentService("FeedService") {

    private val updateHandler = Handler()

    private val TAG = "FeedService"
    private val delayUpdate = 1000L * 60 //every 1 min
    private val minDiffTime = 1000L * 60 * 5

    @Inject lateinit var notification: NotificationCenter
    @Inject lateinit var repository: Repository
    @Inject lateinit var cache: Cache

    private val lastUpdateTime get() = cache.getLastUpdateTime()
    private val currentTime get() = System.currentTimeMillis()

    override fun onCreate() {
        super.onCreate()
        applicationComponent.inject(this)
    }

    override fun onHandleIntent(intent: Intent?) {
        Log.i(TAG, "Start service")
        val isFirst = cache.isFirstLaunch()
        Log.i(TAG, "first time: $isFirst")
        if (isFirst) {
            this.startUpdate()
        }else{
            this.startCheckTimeForUpdate()
        }
    }

    private val checkTimeForUpdate = Runnable {
        val diff = currentTime - lastUpdateTime
        Log.i(TAG, "currentTime: $currentTime, lastUpdateTime: $lastUpdateTime")
        if ((diff / minDiffTime) % 60 > 0)
            this.startUpdate()
        else this.startCheckTimeForUpdate()
    }

    private fun startCheckTimeForUpdate() {
        updateHandler.postDelayed(checkTimeForUpdate, delayUpdate)
    }

    private fun stopCheckTimeForUpdate() {
        updateHandler.removeCallbacks(checkTimeForUpdate)
    }

    private fun startUpdate() {
        stopCheckTimeForUpdate()
        Log.i(TAG, "Start loading feed")
        launch {
            try {
                val feedList = repository.loadFeed()
                cache.putLastUpdateTime(currentTime)
                handleResult(feedList)
                startCheckTimeForUpdate()
            }catch (t: Throwable) {
                Log.e(TAG, "Couldn't load feed", t)
                startCheckTimeForUpdate()
            }
        }
    }

    private fun handleResult(feedList: ArrayList<Feed>) {
        runOnUiThread({
            notification.needUpdateChanges(needUpdateFeed, feedList)
        })
    }
}