package com.nakulov.feed.presentation.view.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.Toast
import com.nakulov.feed.presentation.FeedApp.Companion.applicationComponent
import com.nakulov.feed.presentation.di.HasComponent
import com.nakulov.feed.presentation.di.components.DaggerFragmentComponent
import com.nakulov.feed.presentation.di.components.FragmentComponent
import com.nakulov.feed.presentation.di.modules.FragmentModule
import com.nakulov.feed.presentation.view.activities.LaunchActivity
import kotlinx.android.synthetic.main.activity_launch.container

abstract class BaseFragment : Fragment(), HasComponent<FragmentComponent> {

    protected lateinit var fragmentComponent: FragmentComponent

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.initializeInjector()
        this.fragmentComponent.inject(this)
    }

    private fun initializeInjector() {
        this.fragmentComponent = DaggerFragmentComponent.builder()
                .applicationComponent(applicationComponent)
                .fragmentModule(FragmentModule(this))
                .build()
    }

    protected fun presentFragment(fragment: Fragment, removeLast: Boolean = false) {
        val currentActivity = activity
        if (currentActivity != null && currentActivity is LaunchActivity) run {
            currentActivity.presentFragment(currentActivity.container.id, fragment, removeLast)
        }
    }

    protected fun showToastMessage(message: String) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
    }

    override fun getComponent(): FragmentComponent = this.fragmentComponent
}