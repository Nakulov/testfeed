package com.nakulov.feed.presentation.view

interface LoadDataView {

    fun showLoading()

    fun hideLoading()

    fun showError(message: String)

}