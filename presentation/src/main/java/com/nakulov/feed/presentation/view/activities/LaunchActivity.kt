package com.nakulov.feed.presentation.view.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.nakulov.feed.data.cache.Cache
import com.nakulov.feed.presentation.R.layout
import com.nakulov.feed.presentation.service.FeedService
import com.nakulov.feed.presentation.view.fragments.FeedListFragment
import com.nakulov.feed.presentation.view.fragments.NewsFragment
import com.nakulov.feed.presentation.view.fragments.SplashFragment
import kotlinx.android.synthetic.main.activity_launch.container
import javax.inject.Inject

class LaunchActivity : BaseActivity() {

    @Inject lateinit var cache: Cache

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.setContentView(layout.activity_launch)
        this.activityComponent.inject(this)
        this.initializeView(savedInstanceState)
        this.startFeedService()
    }

    private fun initializeView(savedInstanceState: Bundle?) {
        val isFirstLaunch = cache.isFirstLaunch()

        when{
            isFirstLaunch -> presentFragment(container.id, SplashFragment())
            else -> {
                try {
                    if (savedInstanceState != null) {
                        val fragmentName = savedInstanceState.getString("fragment")
                        val args = savedInstanceState.getBundle("args")
                        when (fragmentName) {
                            "splash" -> presentFragment(container.id, SplashFragment(), true)
                            "feedList" -> presentFragment(container.id, FeedListFragment(), true)
                            "news" -> {
                                val newsFragment = NewsFragment()
                                newsFragment.arguments = args
                                presentFragment(container.id, newsFragment)
                            }
                            else -> initializeMainFragment()
                        }
                    }else {
                        initializeMainFragment()
                    }
                }catch (e: Exception){
                    Log.e("LaunchActivity", "load state exception", e)
                    initializeMainFragment()
                }
            }
        }
    }

    private fun startFeedService() {
        val feedIntent = Intent(Intent.ACTION_SYNC, null, this, FeedService::class.java)
        this.startService(feedIntent)
    }

    private fun initializeMainFragment() {
        this.presentFragment(container.id, FeedListFragment(), true)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        try {
            super.onSaveInstanceState(outState)
            val lastFragment = supportFragmentManager.fragments.last()
            if (lastFragment != null) {
                when (lastFragment) {
                    is FeedListFragment -> outState?.putString("fragment", "feedList")
                    is NewsFragment -> {
                        val args = lastFragment.arguments
                        outState?.putString("fragment", "news")
                        outState?.putBundle("args", args)
                    }
                    is SplashFragment -> outState?.putString("fragment", "splash")
                }
            }
        } catch (e: Exception) {
            Log.e("LaunchActivity", "save state exception", e)
        }
    }
}