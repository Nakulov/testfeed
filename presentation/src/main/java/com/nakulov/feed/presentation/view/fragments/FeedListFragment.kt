package com.nakulov.feed.presentation.view.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nakulov.feed.data.models.Feed
import com.nakulov.feed.presentation.R
import com.nakulov.feed.presentation.presenter.FeedPresenter
import com.nakulov.feed.presentation.view.FeedListView
import com.nakulov.feed.presentation.view.adapter.FeedListAdapter
import kotlinx.android.synthetic.main.feed_list_fragment.favourites
import kotlinx.android.synthetic.main.feed_list_fragment.recyclerFeeds
import kotlinx.android.synthetic.main.feed_list_fragment.refresher
import javax.inject.Inject

class FeedListFragment : BaseFragment(), FeedListView {

    @Inject lateinit var presenter: FeedPresenter
    @Inject lateinit var adapter: FeedListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.fragmentComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.feed_list_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.presenter.attachView(this)
        this.initButtons()
        this.setAdapter()
        this.initialize()
    }

    private fun initialize() {
        this.presenter.initialize()
    }

    private fun initButtons() {
        this.refresher.setOnRefreshListener { presenter.loadFeed() }
        this.adapter.setOnItemClickListener { i, feed ->
            Log.i("FeedListener", "position: $i, link: ${feed.link}")
            val args = Bundle()
            args.putInt("page", i)
            val newsFragment = NewsFragment()
            newsFragment.arguments = args
            presentFragment(newsFragment)
        }
        this.favourites.setOnCheckedChangeListener { buttonView, isChecked ->
            adapter.setFavourites(isChecked)
        }
    }

    private fun setAdapter() {
        this.recyclerFeeds.adapter = adapter
        this.recyclerFeeds.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        this.recyclerFeeds.setHasFixedSize(false)
    }

    override fun renderFeedList() {
        if (adapter.isDataSetChanged) {
            adapter.notifyDataSetChanged()
        }
    }

    override fun showLoading() {
        this.refresher.isRefreshing = true
    }

    override fun hideLoading() {
        this.refresher.isRefreshing = false
    }

    override fun showError(message: String) {
        this.showToastMessage(message)
    }

    override fun onPause() {
        this.presenter.pause()
        super.onPause()
    }

    override fun onResume() {
        this.presenter.resume()
        super.onResume()
    }

    override fun onDestroyView() {
        this.presenter.detachView()
        super.onDestroyView()
    }

    override fun onDestroy() {
        this.adapter.dispose()
        this.presenter.destroy()
        super.onDestroy()
    }
}