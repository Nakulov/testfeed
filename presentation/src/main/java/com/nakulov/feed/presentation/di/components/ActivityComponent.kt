package com.nakulov.feed.presentation.di.components

import android.support.v7.app.AppCompatActivity
import com.nakulov.feed.presentation.di.PerActivity
import com.nakulov.feed.presentation.di.modules.ActivityModule
import com.nakulov.feed.presentation.view.activities.BaseActivity
import com.nakulov.feed.presentation.view.activities.LaunchActivity
import dagger.Component

@PerActivity
@Component(dependencies = [(ApplicationComponent::class)], modules = [(ActivityModule::class)])
interface ActivityComponent {

    fun activity(): AppCompatActivity

    fun inject(activity: BaseActivity)

    fun inject(launchActivity: LaunchActivity)

}