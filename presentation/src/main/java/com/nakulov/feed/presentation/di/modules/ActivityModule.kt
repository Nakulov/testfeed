package com.nakulov.feed.presentation.di.modules

import android.support.v7.app.AppCompatActivity
import com.nakulov.feed.presentation.di.PerActivity
import dagger.Module
import dagger.Provides

@Module
class ActivityModule (private val activity: AppCompatActivity){

    @Provides
    @PerActivity
    fun activity(): AppCompatActivity = this.activity

}