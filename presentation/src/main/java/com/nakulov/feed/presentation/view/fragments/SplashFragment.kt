package com.nakulov.feed.presentation.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nakulov.feed.presentation.R
import com.nakulov.feed.presentation.presenter.SplashPresenter
import com.nakulov.feed.presentation.view.SplashView
import javax.inject.Inject

class SplashFragment : BaseFragment(), SplashView{

    @Inject lateinit var presenter: SplashPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.fragmentComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.splash_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.presenter.attachView(this)
    }

    override fun showFeed() {
        presentFragment(FeedListFragment(), true)
    }

    override fun onPause() {
        this.presenter.pause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        this.presenter.resume()
    }

    override fun onDestroyView() {
        this.presenter.detachView()
        super.onDestroyView()
    }

    override fun onDestroy() {
        this.presenter.destroy()
        super.onDestroy()
    }

    override fun hideLoading() {

    }

    override fun showLoading() {

    }

    override fun showError(message: String) {
        this.showToastMessage(message)
    }
}