package com.nakulov.feed.presentation.di.modules

import android.content.Context
import android.content.res.Resources
import com.nakulov.feed.data.NotificationCenter
import com.nakulov.feed.data.NotificationCenterImpl
import com.nakulov.feed.data.cache.Cache
import com.nakulov.feed.data.cache.CacheImpl
import com.nakulov.feed.data.net.DataRepository
import com.nakulov.feed.data.net.Repository
import com.nakulov.feed.presentation.FeedApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(private val application: FeedApp) {

    @Provides
    @Singleton
    fun provideApplicationContext() : Context = this.application

    @Provides
    @Singleton
    fun provideApplicationResource() : Resources = this.application.resources

    @Provides
    @Singleton
    fun provideCache(cacheImpl: CacheImpl) : Cache = cacheImpl

    @Provides
    @Singleton
    fun provideDataRepository(dataRepository: DataRepository) : Repository = dataRepository

    @Provides
    @Singleton
    fun provideNotificationCenter(notificationCenter: NotificationCenterImpl) : NotificationCenter = notificationCenter

}