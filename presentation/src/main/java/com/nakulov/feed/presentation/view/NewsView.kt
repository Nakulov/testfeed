package com.nakulov.feed.presentation.view

import io.realm.Realm

interface NewsView : LoadDataView{

    fun setNews(news: Collection<String>)

    fun setRealm(realm: Realm)

}