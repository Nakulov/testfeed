package com.nakulov.feed.presentation.di.components

import com.nakulov.feed.presentation.di.PerFragment
import com.nakulov.feed.presentation.di.modules.FragmentModule
import com.nakulov.feed.presentation.view.fragments.BaseFragment
import com.nakulov.feed.presentation.view.fragments.FeedListFragment
import com.nakulov.feed.presentation.view.fragments.NewsFragment
import com.nakulov.feed.presentation.view.fragments.SplashFragment
import dagger.Component

@PerFragment
@Component(dependencies = [(ApplicationComponent::class)], modules = [(FragmentModule::class)])
interface FragmentComponent {

    fun inject(baseFragment: BaseFragment)

    fun inject(splashFragment: SplashFragment)

    fun inject(feedListFragment: FeedListFragment)

    fun inject(newsFragment: NewsFragment)

}