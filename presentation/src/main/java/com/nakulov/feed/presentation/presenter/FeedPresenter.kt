package com.nakulov.feed.presentation.presenter

import android.util.Log
import com.nakulov.feed.data.NotificationCenter
import com.nakulov.feed.data.NotificationCenterImpl.NotificationObserverDelegate
import com.nakulov.feed.data.models.Feed
import com.nakulov.feed.data.needUpdateFeed
import com.nakulov.feed.data.net.Repository
import com.nakulov.feed.presentation.di.PerFragment
import com.nakulov.feed.presentation.view.FeedListView
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import javax.inject.Inject

@PerFragment
class FeedPresenter @Inject constructor(
        private val notification: NotificationCenter,
        private val repository: Repository
) : Presenter, NotificationObserverDelegate {

    private var feedListView: FeedListView? = null

    fun attachView(feedListView: FeedListView) {
        this.feedListView = feedListView
        this.addObservers()
    }

    fun initialize() {

    }

    private fun addObservers() {
        this.notification.addObserver(needUpdateFeed, this)
    }

    fun loadFeed() {
        showViewLoading()
        launch(UI) {
            try {
                repository.loadFeed()
                showFeedList()
                hideViewLoading()
            } catch (t: Throwable) {
                Log.e("Feed", "couldn't load feed", t)
                showErrorMessage(t.message ?: "couldn't load feed")
                hideViewLoading()
            }
        }
    }

    @Suppress("UNCHECKED_CAST")
    override fun notifyObserver(id: Int, vararg arguments: Any) {
        when (id) {
            needUpdateFeed -> {
                val feedList = arguments[0] as? ArrayList<Feed>
                if (feedList != null && feedList.isNotEmpty()) {
                    showFeedList()
                }
            }
        }
    }

    private fun showFeedList() {
        this.feedListView?.renderFeedList()
    }

    private fun showViewLoading() {
        this.feedListView?.showLoading()
    }

    private fun hideViewLoading() {
        this.feedListView?.hideLoading()
    }

    private fun showErrorMessage(message: String) {
        this.feedListView?.showError(message)
    }

    override fun pause() {

    }

    override fun resume() {

    }

    private fun removeObservers() {
        this.notification.removeObserver(needUpdateFeed, this)
    }

    fun detachView() {
        this.removeObservers()
        this.feedListView = null
    }

    override fun destroy() {

    }
}