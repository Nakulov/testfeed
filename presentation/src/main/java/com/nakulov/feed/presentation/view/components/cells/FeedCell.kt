package com.nakulov.feed.presentation.view.components.cells

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Paint.Style.FILL
import android.graphics.RectF
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.nakulov.feed.data.models.Feed
import com.nakulov.feed.presentation.MATCH_PARENT
import com.nakulov.feed.presentation.R
import com.nakulov.feed.presentation.WRAP_CONTENT
import com.nakulov.feed.presentation.dip

class FeedCell @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private val backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = FILL
        color = Color.WHITE
    }

    private val backgroundRect = RectF()

    private val titleView = TextView(context).apply {
        setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16f)
        setTextColor(Color.BLACK)
    }
    private val descriptionView = TextView(context).apply {
        setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12f)
        setTextColor(Color.BLACK)
    }

    private val dateView = TextView(context).apply {
        setTextSize(TypedValue.COMPLEX_UNIT_DIP, 12f)
        setTextColor(Color.BLACK)

        movementMethod = LinkMovementMethod.getInstance()
    }

    val buttonRead = Button(context).apply {
        setAllCaps(false)
        setText(R.string.button_read)
        setTextColor(0xffb5b7bc.toInt())
        setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14f)
        setBackgroundResource(R.drawable.button_read)
    }

    init {
        setPadding(8.dip(), 8.dip(), 8.dip(), 8.dip())
        setWillNotDraw(false)
        orientation = VERTICAL

        addView(titleView, LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            gravity = Gravity.START or Gravity.TOP
        })
        addView(dateView, LayoutParams(WRAP_CONTENT, WRAP_CONTENT).apply {
            gravity = Gravity.END or Gravity.TOP
        })
        addView(descriptionView, LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
            topMargin = 4.dip()
            gravity = Gravity.CENTER_HORIZONTAL or Gravity.TOP
        })
        addView(buttonRead, LayoutParams(MATCH_PARENT, 40.dip()).apply {
            setMargins(16.dip(), 8.dip(), 16.dip(), 0)
        })
    }

    fun setFeed(feed: Feed) {
        this.titleView.text = feed.title

        val description = if (VERSION.SDK_INT >= VERSION_CODES.N) {
            Html.fromHtml(feed.description, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(feed.description)
        }
        this.descriptionView.text = description

        this.dateView.text = feed.date
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.run {
            backgroundRect.set(0f, 0f, canvas.width.toFloat(), canvas.height.toFloat())
            canvas.drawRoundRect(backgroundRect, 5f.dip(), 5f.dip(), backgroundPaint)
        }
    }
}