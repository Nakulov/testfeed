package com.nakulov.feed.presentation.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.nakulov.feed.presentation.R
import com.nakulov.feed.presentation.presenter.NewsPresenter
import com.nakulov.feed.presentation.view.NewsView
import io.realm.Realm
import kotlinx.android.synthetic.main.news_fragment.newsReader
import javax.inject.Inject

class NewsFragment : BaseFragment(), NewsView {

    @Inject lateinit var presenter: NewsPresenter

    private var page = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.fragmentComponent.inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.news_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.restoreFromBundle(arguments)
        this.presenter.attachView(this)
        this.presenter.initialize()
    }

    override fun setNews(news: Collection<String>) {
        this.newsReader.setLinks(news)
        this.newsReader.setPage(page)
    }

    override fun setRealm(realm: Realm) {
        this.newsReader.setRealm(realm)
    }

    override fun showLoading() {

    }

    override fun hideLoading() {

    }

    override fun showError(message: String) {
        this.showToastMessage(message)
    }

    private fun restoreFromBundle(bundle: Bundle?) {
        bundle?.run {
            page = bundle.getInt("page", 0)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("page", page)
    }

    override fun onPause() {
        this.presenter.pause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        this.presenter.resume()
    }

    override fun onDestroyView() {
        this.presenter.detachView()
        super.onDestroyView()
    }

    override fun onDestroy() {
        this.presenter.destroy()
        super.onDestroy()
    }
}