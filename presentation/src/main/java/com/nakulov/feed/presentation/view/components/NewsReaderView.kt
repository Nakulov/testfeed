package com.nakulov.feed.presentation.view.components

import android.annotation.SuppressLint
import android.content.Context
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.FrameLayout
import com.nakulov.feed.data.models.News
import com.nakulov.feed.presentation.MATCH_PARENT
import io.realm.Realm

class NewsReaderView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {

    private var realm: Realm? = null

    private val viewPager = ViewPager(context)
    private val adapter = NewsAdapter()
    private val webEntities = arrayListOf<String>()

    init {
        viewPager.adapter = adapter
        viewPager.pageMargin = 0
        viewPager.offscreenPageLimit = 1
        addView(viewPager, MATCH_PARENT, MATCH_PARENT)
    }

    fun setLinks(links: Collection<String>) {
        this.webEntities.addAll(links)
        this.adapter.notifyDataSetChanged()
    }

    fun setRealm(realm: Realm) {
        this.realm = realm
    }

    fun setPage(page: Int) {
        this.viewPager.currentItem = if (page in 0 until webEntities.size) page else 0
    }

    private inner class NewsAdapter : PagerAdapter() {

        @SuppressLint("SetJavaScriptEnabled")
        override fun instantiateItem(container: ViewGroup, position: Int): Any {

            val webEntity = webEntities[position]

            val webView = WebView(context)
            webView.settings.domStorageEnabled = true
            webView.settings.javaScriptEnabled = true
            webView.settings.loadWithOverviewMode = true
            webView.settings.useWideViewPort = true
            webView.settings.builtInZoomControls = true
            webView.settings.displayZoomControls = false
            webView.setInitialScale(1)
            webView.settings.allowFileAccess = true
            webView.settings.cacheMode = WebSettings.LOAD_DEFAULT
            webView.settings.setAppCacheEnabled(true)
            webView.settings.setAppCachePath(context.cacheDir.absolutePath)
            webView.webChromeClient =
                    WebChromeClient()
            webView.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)

                    if (url != null && url.isNotEmpty()) {
                        val realm = Realm.getDefaultInstance()
                        val news = News(url)
                        realm.executeTransactionAsync { it.copyToRealmOrUpdate(news) }
                    }
                }
            }
            webView.loadUrl(webEntity)

            container.addView(webView, 0)

            return webView
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

        override fun getCount(): Int = webEntities.size

        override fun restoreState(state: Parcelable?, loader: ClassLoader?) {

        }

        override fun saveState(): Parcelable? = null

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            try {
                val webView = `object` as WebView
                webView.stopLoading()
                webView.loadUrl("about:blank")
                webView.destroy()
            }catch (t: Throwable) {

            }
            container.removeView(`object` as View)
        }
    }

}