package com.nakulov.feed.presentation.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.nakulov.feed.data.models.Feed
import com.nakulov.feed.data.models.News
import com.nakulov.feed.presentation.MATCH_PARENT
import com.nakulov.feed.presentation.WRAP_CONTENT
import com.nakulov.feed.presentation.dip
import com.nakulov.feed.presentation.view.components.cells.FeedCell
import io.realm.Realm
import javax.inject.Inject

class FeedListAdapter @Inject constructor(
        private val context: Context
) : RecyclerView.Adapter<ViewHolder>() {

    interface ItemClickListener {

        fun onItemClick(position: Int, entity: Feed)

    }

    private var favouritesMode = false
    private val realm = Realm.getDefaultInstance()
    private var itemClickListener: ItemClickListener? = null

    private val feedList: ArrayList<Feed>
        get() {
            val list = arrayListOf<Feed>()
            if (favouritesMode) {
                val cachedLinks = realm.where(News::class.java).findAll().map{it.link}.toTypedArray()
                realm.where(Feed::class.java).`in`("link", cachedLinks).findAll().toCollection(list)
            } else realm.where(Feed::class.java).findAll().toCollection(list)
            return list
        }

    private var currentCount = 0
    val isDataSetChanged: Boolean
        get() {
            val current = currentCount

            return current != itemCount || current == 1
        }

    fun setFavourites(favouritesMode: Boolean) {
        this.favouritesMode = favouritesMode
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val cell = FeedCell(context)

        cell.layoutParams = RecyclerView.LayoutParams(MATCH_PARENT, WRAP_CONTENT).apply {
            topMargin = 4.dip()
            bottomMargin = 4.dip()
        }

        return ViewHolder(cell)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val feed = feedList[position]

        val cell = holder.itemView as? FeedCell ?: return

        cell.setFeed(feed)
        cell.buttonRead.setOnClickListener { itemClickListener?.onItemClick(position, feed) }
    }

    override fun getItemCount(): Int {
        val count = feedList.size

        currentCount = count

        return count
    }

    inline fun setOnItemClickListener(crossinline getter: (Int, Feed) -> Unit) {
        val itemClickListener = object : ItemClickListener {
            override fun onItemClick(position: Int, entity: Feed) {
                getter(position, entity)
            }
        }
        this.setOnItemClickListener(itemClickListener)
    }

    fun setOnItemClickListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }

    fun dispose() {
        this.itemClickListener = null
        this.realm.close()
    }
}

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)