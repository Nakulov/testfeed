package com.nakulov.feed.data.net

import com.nakulov.feed.data.models.Feed
import com.nakulov.feed.data.transformFeedList
import io.realm.Realm
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataRepository @Inject constructor(
        private val apiConnection: ApiConnection
) : Repository {

    suspend override fun loadFeed(): ArrayList<Feed> {
        val realm = Realm.getDefaultInstance()
        val rssFeed = apiConnection.service.loadFeed().await()

        val feedList = transformFeedList(rssFeed)

        realm.use {
            it.executeTransaction { it.copyToRealmOrUpdate(feedList) }
        }

        return feedList
    }
}