package com.nakulov.feed.data.net

import me.toptas.rssconverter.RssFeed
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiRepository {

    @GET("_/rss/_rss.html")
    fun loadFeed(@Query("subtype") subtype: Int = 1, @Query("category") category: Int = 2, @Query("city") city: Int = 21) : Call<RssFeed>

}