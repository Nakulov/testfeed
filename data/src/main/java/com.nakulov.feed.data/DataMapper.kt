package com.nakulov.feed.data

import com.nakulov.feed.data.models.Feed
import me.toptas.rssconverter.RssFeed
import me.toptas.rssconverter.RssItem

fun transformFeedList(rssFeed: RssFeed): ArrayList<Feed> =
        rssFeed.items.map { transformFeed(it) }.toCollection(arrayListOf())

fun transformFeed(rssItem: RssItem): Feed =
        Feed(
                rssItem.publishDate,
                rssItem.title,
                rssItem.link,
                rssItem.description
        )
