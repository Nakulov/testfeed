package com.nakulov.feed.data

import android.os.Looper
import android.util.SparseArray
import javax.inject.Inject
import javax.inject.Singleton

private var currentId = 0

val needUpdateFeed = currentId++

@Singleton
class NotificationCenterImpl @Inject constructor() : NotificationCenter {

    interface NotificationObserverDelegate {
        fun notifyObserver(id: Int, vararg arguments: Any)
    }

    private val observers = SparseArray<ArrayList<Any>>()

    override fun addObserver(id: Int, observer: Any) {
        var objects = observers[id]

        if (objects == null){
            objects = ArrayList()
            observers.put(id, objects)
        }
        if (objects.contains(observer)) return

        objects.add(observer)
    }

    override fun removeObserver(id: Int, observer: Any) {
        val objects = observers[id] ?: return

        if (objects.contains(observer))
            objects.remove(observer)
    }

    override fun needUpdateChanges(id: Int, vararg arguments: Any) {
        if (Thread.currentThread() != Looper.getMainLooper().thread) return

        val objects = observers[id] ?: return

        if (objects.isNotEmpty()) {
            (0 until objects.size)
                    .map { objects[it] }
                    .forEach { (it as? NotificationObserverDelegate)?.notifyObserver(id, *arguments) }
        }
    }

}

interface NotificationCenter {

    fun addObserver(id: Int, observer: Any)

    fun removeObserver(id: Int, observer: Any)

    fun needUpdateChanges(id: Int, vararg arguments: Any)
}