package com.nakulov.feed.data.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class Feed(
        @PrimaryKey
        open var date: String = "",
        open var title: String = "",
        open var link: String = "",
        open var description: String = ""
) : RealmObject()

@RealmClass
open class News(
        @PrimaryKey
        open var link: String = ""
) : RealmObject()